<?php
// src/Controller/PlanetsController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;

class PlanetInfoController extends AbstractController
{
    public function getInfoData(int $id): Array {
      $httpClient = HttpClient::create();
      $response = $httpClient->request('GET', 'https://swapi.dev/api/planets/'.$id.'/');
      $statusCode = $response->getStatusCode();

      if ($statusCode == 200) {
        $data = json_decode($response->getContent());

        return [
          'id' => $id,
          'name' => $data->name,
          'rotation_period' => $data->rotation_period,
          'orbital_period' => $data->orbital_period,
          'diameter' => $data->diameter,
          'films_count' => count($data->films),
          'created' => $data->created,
          'edited' => $data->edited,
          'url' => $data->url
        ];
      }

      return [];
    }

    /**
     * @Route("/planets/{id}", name="planet_info", methods={"GET"})
     */
    public function info(int $id): Response
    {
      $infoData = $this->getInfoData($id);

      if (count($infoData) > 0) {
        return $this->json($infoData);
      }

      return new Response('<html><body><b>Error: The Planet Does Not Exists</b></body></html>');
    }
}
