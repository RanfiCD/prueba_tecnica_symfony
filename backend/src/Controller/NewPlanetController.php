<?php
// src/Controller/PlanetsController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Validator\Constraints\NotBlank;

use App\Entity\Planet;

class NewPlanetController extends AbstractController
{
    /**
     * @Route("/planet", name="planet_create", methods={"POST"})
     */
    public function create(PlanetInfoController $planetInfo): Response
    {
      $request = Request::createFromGlobals();

      $defaultData = [
        'rotation_period' => null,
        'orbital_period' => null,
        'diameter' => null
      ];
      $form = $this->createFormBuilder($defaultData)
        ->add('id', Type\NumberType::class, ['constraints' => new NotBlank()])
        ->add('name', Type\TextType::class, ['constraints' => new NotBlank()])
        ->add('rotation_period', Type\NumberType::class)
        ->add('orbital_period', Type\NumberType::class)
        ->add('diameter', Type\NumberType::class)
        ->getForm();
      
      $form->handleRequest($request);

      if(!$form->isSubmitted()){
        $form->submit($request->request->all());
      }

      $message = 'Error: Invalid Params';
      if ($form->isValid()) {
        $entityManager = $this->getDoctrine()->getManager();
        $planet = $entityManager->getRepository(Planet::class)->find($request->request->get('id'));

        $message = 'Error: Planet Already Exists';
        if (!$planet) {
          $planetData = $planetInfo->getInfoData($request->request->get('id'));

          if (count($planetData) == 0) {
            return new Response('<html><body><b>Error: Invalid Planet</b></body></html>');
          }
          
          $planet = new Planet();
          $planet->setApiId($planetData['id']);
          $planet->setName($planetData['name']);
          $planet->setRotationPeriod($planetData['rotation_period']);
          $planet->setOrbitalPeriod($planetData['orbital_period']);
          $planet->setDiameter($planetData['diameter']);
          $planet->setFilmsCount($planetData['films_count']);
          $planet->setCreated(new \DateTime());
          $planet->setEdited(new \DateTime());
          $planet->setUrl($planetData['url']);

          $entityManager->persist($planet);
          $entityManager-> flush();

          return $this->render('planet.html.twig', ['planet' => $planetData]);
        }
      }

      return new Response(
        '<html><body><b>'.$message.'</b></body></html>'
      );
    }
}
